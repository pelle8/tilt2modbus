#!/usr/bin/env python
'''
Pymodbus Synchronous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

Modified to connect to Tilt2mqtt, Per Carlen
'''
from pymodbus.client import ModbusTcpClient as ModbusClient

import logging

def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")


def tilt():
  client = ModbusClient('192.168.2.134', port=1502)

  rr = client.write_registers(200, [5,6,7], 0x01)
  rr = client.read_input_registers( 100, 20, 1)
  print(rr.registers)

  client.close()


tilt()

