#!/usr/bin/env python
"""
- Modbus server for tilt sensor // Per Carlen
"""
from typing import Any, Dict
import os
import time
import json
import yaml
import copy
import threading
import importlib
from typing import Callable
import asyncio

queue = asyncio.Queue()

class ModuleThread:
    def __init__(self, name: str, target: Callable):
        self._name = name
        self._alive = False
        self.running = False
        self._target = target
        self._thread = None

    def start_thread(self):
        if not self._alive:
            self._thread = threading.Thread(name=self._name, target=self._run, daemon=True)
            self._thread.start()

    def stop_thread(self):
        if self._alive:
            self.running = False

    def start_stop_thread(self):
        if self._alive:
            self.stop_thread()
        else:
            self.start_thread()

    def _run(self):
        self._alive = True
        self.running = True
        self._target()
        self._alive = False

    def is_alive(self):
        return self._alive


class ModbusHandler:
  def __init__(self):
    print("\nStarting modbus_handler")
    self.config = self.read_config_file('config.yaml')
    #print(self.config)
    print("Starting thread for modbus server")
    self.thread: ModuleThread = ModuleThread('mqtt2modbus', self.run)
    self.thread.start_thread()

  def run(self):
    while self.thread.running:
      print("Loading module for modbus")
      mqtt2modbus = importlib.import_module('mqtt2modbus', package=None)
      mqtt2modbus.run_updating_server(self,queue)
      time.sleep(1)

  @staticmethod
  def read_config_file(filename: str) -> Dict:

    config = ""
    filename = "tilt2modbus.yaml"
    path = "/etc/tilt2modbus/"
    if os.path.isfile(path+filename):
      filename = path + filename
    print("Reading from:",filename)
    try:
      with open(filename,'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ",filename)
      exit(1)
    return config

  def loop(self):
    try:
      while True:
        time.sleep(2)
    except KeyboardInterrupt:
      print("Done!")
      pass

if __name__ == '__main__':
    modbus_handler = ModbusHandler()
    modbus_handler.loop()
