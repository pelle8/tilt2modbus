#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#
# Receives tilt data from mqtt, puts into modbus registers
#
# 2023 Per Carlen
#
import sys
import asyncio
import re

import os
import yaml
import json
from typing import Dict
import paho.mqtt.client as paho

from pymodbus.datastore import ModbusSlaveContext, ModbusServerContext
from pymodbus.datastore import ModbusSequentialDataBlock, ModbusSparseDataBlock

from pymodbus.device import ModbusDeviceIdentification
from pymodbus.server import StartAsyncSerialServer
# --------------------------------------------------------------------------- #
# import the modbus libraries we need
# --------------------------------------------------------------------------- #
from pymodbus.server import StartAsyncTcpServer
from pymodbus.transaction import ModbusRtuFramer


# global

class Tilt2Modbus:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')

  @staticmethod
  def read_config_file(filename: str) -> Dict:
    config = ""
    filename = "tilt2modbus.yaml"
    path = "/etc/tilt2modbus/"
    if os.path.isfile(path + filename):
      filename = path + filename
    print("Reading from:", filename)
    try:
      with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ", filename)
      exit(1)
    return config


class CallbackDataBlock(ModbusSequentialDataBlock):
    """A datablock that stores the new value in memory,

    and passes the operation to a message queue for further processing.
    """

    def __init__(self, queue, addr, values):
        """Initialize."""
        self.queue = queue
        super().__init__(addr, values)

    def setValues(self, address, value):
        global config
        """Set the requested values of the datastore."""
        super().setValues(address, value)
        txt = f"Callback from setValues with address {address}, value {value}"
        if config['debug']: print(txt)

    def getValues(self, address, count=1):
        global config
        """Return the requested values from the datastore."""
        result = super().getValues(address, count=count)
        txt = f"Callback from getValues with address {address}, count {count}, data {result}"
        if config['debug']: print(txt)
        return result

    def validate(self, address, count=1):
        """Check to see if the request is in range."""
        result = super().validate(address, count=count)
        txt = f"Callback from validate with address {address}, count {count}, data {result}"
        return result


def init_datastore(queue):
    # ----------------------------------------------------------------------- #
    # initialize your data store
    # ----------------------------------------------------------------------- #
    id = 0
    block = CallbackDataBlock(queue, 0x00, [0] * 300)

    store = ModbusSlaveContext(di=block, co=block, hr=block, ir=block, unit=1)
    context = ModbusServerContext(slaves=store, single=True)

    return context



def mqtt_on_connect(client, userdata, flags, rc):
  if rc != 0:
    print("MQTT connection problem")
    client.connected_flag = False
  else:
    print("MQTT client connected:" + str(client))
    client.connected_flag = True

def mqtt_on_subscribe(client, userdata, mid, granted_qos):
    # print(" Received message " + str(client)
    #    + "' with QoS " + str(granted_qos))
    pass

def mqtt_on_message(mqtt_client, userdata, message, tmp=None):
    global config

    if config['debug']:
        print(" Received message " + str(message.payload)
              + " on topic '" + message.topic
              + "' with QoS " + str(message.qos))

    json_data = json.loads(message.payload)
    if type(json_data) is dict or type(json_data) is list:
        rc = insert_data_in_modbus_registers(json_data)

def encode_value_to_32bits(v):
    a = abs(v) // 65536
    b = abs(v - a * 65536)
    if v < 0:
        b = 65535 - b
        a = 65535 - a
    return [int(a), int(b)]

def encode_value_to_16bits(v):
    if abs(v) > 32767: # Max value
      v = 0
    if v < 0:
        a = 65535 + v
    else: 
        a = v
    return [int(a)]


def insert_data_in_modbus_registers(tilt_dict):
  global context

  id = int(tilt_dict['id'])
  if id < 999:
    address_offset = (id - 1) * 10 + 100
    context[0].setValues(3, address_offset + 0, encode_value_to_16bits(int(100 * tilt_dict['major'])) )
    context[0].setValues(3, address_offset + 1, encode_value_to_16bits(int(1000 * tilt_dict['minor'])) )
    context[0].setValues(3, address_offset + 2, encode_value_to_16bits(1 * int(tilt_dict['tx_power'])) )
    context[0].setValues(3, address_offset + 3, encode_value_to_16bits(1 * int(tilt_dict['rssi'])) )


def run_mqtt_setup(config):
  global mqtt_client1

  if "mqtt_broker" in config:
    mqtt_broker = config['mqtt_broker']
    mqtt_port = config['mqtt_port']
    mqtt_username = config['mqtt_username']
    mqtt_password = config['mqtt_password']
  else:
    exit(1)

  print("\nStarting mqtt handler")
  # this mqtt client will publish
  mqtt_client1 = paho.Client(client_id="tilt_subscriber", transport="tcp", protocol=paho.MQTTv311,
                             clean_session=True)
  mqtt_client1.on_subscribe = mqtt_on_subscribe
  mqtt_client1.on_connect = mqtt_on_connect
  mqtt_client1.on_message = mqtt_on_message
  mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
  mqtt_client1.connect(mqtt_broker, mqtt_port, keepalive=60)
  mqtt_client1.subscribe("tilt/data", 2)
  mqtt_client1.loop_start()

async def amain(args=None):
    global config

    print("Reading config from file")
    tilt2modbus = Tilt2Modbus()
    config = tilt2modbus.config
    run_mqtt_setup(config)

    try:
        while True:
            await asyncio.sleep(3600)
    except KeyboardInterrupt:
        print("keyboard interrupt")
    finally:
        print("closing event loop")

def run_updating_server(class_ModbusHandler,queue):

    global ModbusHandler
    global mqtt_client1
    global context
    global config

    ModbusHandler = class_ModbusHandler
    config = ModbusHandler.config
    run_mqtt_setup(config)
    modbus_interface = config['modbus_interface']

    print("\nStarting modbus module")

    # ----------------------------------------------------------------------- # 
    # initialize the server information
    # ----------------------------------------------------------------------- # 
    identity = ModbusDeviceIdentification()
    identity.VendorName = 'pymodbus'
    identity.ProductCode = 'PM'
    identity.VendorUrl = 'http://github.com/riptideio/pymodbus/'
    identity.ProductName = 'pymodbus Server'
    identity.ModelName = 'pymodbus Server'

    print("init datastore")
    context = init_datastore(queue)
    print("Starting modbus server")
    asyncio.run(run_async_server(context, identity, modbus_interface), debug=True)
    print("Server ended")
    exit(1)


async def run_async_server(context, identity, inverter_interface):
    #print("Register task for inverter write check")
    #asyncio.create_task(check_registers(context))
    print(" modbus server at: ", inverter_interface)
    if inverter_interface == "tcp":
        await StartAsyncTcpServer(context=context, identity=identity, address=("0.0.0.0", "1502"))
    else:
        await StartAsyncSerialServer(context=context, framer=ModbusRtuFramer, identity=identity,
                                     port=inverter_interface, timeout=.005, baudrate=9600)

    await StartAsyncSerialServer(context=context, framer=ModbusRtuFramer, identity=identity, port=inverter_interface, timeout=.005, baudrate=9600)



if __name__ == "__main__":
    asyncio.run(amain())
