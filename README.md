# Tilt2modbus

Get bluetooth tiltsensor data into modbus registers, that can be read by a PLC.


## Web interface


![Tilt2modbus](./images/web.png "status web")



## Read Registers

```
101 - Sensor 1 - Temperature in C /100 (signed)
102 - Sensor 1 - Gravity in /1000 (unsigned)
103 - Sensor 1 - tx_power (signed)
104 - Sensor 1 - rssi (signed)
105 - Sensor 1 - mac, octet 1-2
106 - Sensor 1 - mac, octet 3-4
107 - Sensor 1 - mac, octet 5-6
108 - Spare
109 - Spare
110 - Spare

111 - Sensor 2 - Temperature in C /100 (signed)
112 - Sensor 2 - Gravity in /1000 (unsigned)
113 - Sensor 2 - tx_power (signed)
114 - Sensor 2 - rssi (signed)
115 - Sensor 2 - mac, octet 1-2
116 - Sensor 2 - mac, octet 3-4
117 - Sensor 2 - mac, octet 5-6
118 - Spare
119 - Spare
120 - Spare

....
```

## Write Registers

```
To be defined. Status of relays etc? For presentation on rpi webpage.

200-300

....
```

## Modbus reading

Example reading modbus tcp:
```
$ python3 modbus_tcp_client.py
[2500, 1082, 65432, 65473, 0, 0, 0, 0, 0, 0, 2111, 1083, 65432, 65473, 0, 0, 0, 0, 0, 0]
```
