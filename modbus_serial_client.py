#!/usr/bin/env python
'''
Pymodbus Synchronous Client Examples
--------------------------------------------------------------------------

The following is an example of how to use the synchronous modbus client
implementation from pymodbus.

Modified to connect to Tilt2mqtt, Per Carlen
'''
from pymodbus.client import ModbusTcpClient as ModbusClient
from pymodbus.client import AsyncModbusSerialClient
from pymodbus.client import ModbusSerialClient

import asyncio
import logging

def print_rr(lbl,rr):
  print(lbl + ":",end='')
  for x in rr.registers:
    s = x.to_bytes(2,"big").hex()
    print(s + " (" + str(x) + ")",end='')
  print("")


async def tilt():
#  client = ModbusClient('192.168.2.134', port=1502)

#  client = AsyncModbusSerialClient(
  client = ModbusSerialClient(
      method='rtu',
      port="/dev/ttyACM0",
      baudrate=9600,
      timeout=1,
      parity='N',
      stopbits=1,
      bytesize=8
  )

  client.connect()
  # test client is connected
  #assert client.connected
  rr = client.write_registers(200, [5,6,7], 0x01)
  rr = client.read_input_registers( 100, 20, 1)
  print(rr.registers)

  client.close()

if __name__ == "__main__":
    asyncio.run(tilt())


