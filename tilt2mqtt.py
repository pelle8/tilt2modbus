#!/usr/bin/env python3
# -*- coding:utf-8 -*-
#
# This application is an example on how to use aioblescan
#
# Copyright (c) 2017 François Wautier
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
# of the Software, and to permit persons to whom the Software is furnished to do so,
# subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies
# or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
# WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR
# IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE
import sys
import asyncio
import argparse
import re
import aioblescan as aiobs
from aioblescan.plugins import EddyStone
from aioblescan.plugins import RuuviWeather
from aioblescan.plugins import ATCMiThermometer
from aioblescan.plugins import ThermoBeacon
from aioblescan.plugins import Tilt

import os
import yaml
import json
from typing import Dict
import paho.mqtt.client as paho
import time

# global
opts = None
decoders = []

class Tilt2Modbus:
  def __init__(self):
    self.config = self.read_config_file('config.yaml')

  @staticmethod
  def read_config_file(filename: str) -> Dict:
    config = ""
    filename = "tilt2modbus.yaml"
    path = "/etc/tilt2modbus/"
    if os.path.isfile(path + filename):
      filename = path + filename
    print("Reading from:", filename)
    try:
      with open(filename, 'r') as file:
        config = yaml.safe_load(file)
    except:
      print("ERROR: No config file - ", filename)
      exit(1)
    return config



def mqtt_on_connect(client, userdata, flags, rc):
  if rc != 0:
    print("MQTT connection problem")
    client.connected_flag = False
  else:
    print("MQTT client connected:" + str(client))
    client.connected_flag = True


def mqtt_on_publish(mqtt_client, userdata, result):
  pass


def publish(mqtt_client, pub_var, topic):
  #json_string = json.dumps(pub_var)
  json_string = (pub_var)
  print("Publishing:",json_string)
  ret = mqtt_client.publish(topic, json_string)


def run_mqtt_setup(config):
    global mqtt_client1

    if "mqtt_broker" in config:
        mqtt_broker = config['mqtt_broker']
        mqtt_port = config['mqtt_port']
        mqtt_username = config['mqtt_username']
        mqtt_password = config['mqtt_password']
    else:
        exit(1)

    print("\nStarting mqtt handler")
    # this mqtt client will publish
    mqtt_client1 = paho.Client(client_id="tilt_publisher", transport="tcp", protocol=paho.MQTTv311,
                                clean_session=True)
    mqtt_client1.on_publish = mqtt_on_publish
    mqtt_client1.on_connect = mqtt_on_connect
    mqtt_client1.username_pw_set(mqtt_username, mqtt_password)
    mqtt_client1.connect(mqtt_broker, mqtt_port, keepalive=60)
    mqtt_client1.connected_flag = False
    mqtt_client1.loop_start()



def check_mac(val):
    try:
        if re.match("[0-9a-f]{2}([-:])[0-9a-f]{2}(\\1[0-9a-f]{2}){4}$", val.lower()):
            return val.lower()
    except:
        pass
    raise argparse.ArgumentTypeError("%s is not a MAC address" % val)


#tilt_sensors:
#  - id: 1
#    mac: "f9:8c:ac:b8:2c:f9"

def check_tilt_data(tilt_data):
    global config
    global mqtt_client1

    tilt_dict = json.loads(tilt_data)
    id = 99
    for sensor in config['tilt_sensors']:
        if sensor['mac'] == tilt_dict['mac']:
            id = sensor['id']
    tilt_dict['id'] = id
    tilt_dict['last_seen'] = str(int(time.time()))
    if int(tilt_dict['major']) != 999: # 999 <> no temperature value yet
        tilt_dict['major'] = round( (( int(tilt_dict['major']) -32 ) * 5/9),2)
    tilt_dict['minor'] = round( ( int(tilt_dict['minor']) /1000 ),3)
    json_string = json.dumps(tilt_dict)
    if config['debug']: print("Publishing:",json_string)
    return publish(mqtt_client1,json_string,"tilt/data")


def do_tests(test_number):
    global mqtt_client1

    while not mqtt_client1.connected_flag:
      print("Waiting for MQTT connection")
      time.sleep(1)

    if int(test_number) == 1: #known mac
        test_data_1 ='{"uuid": "a495bb10c5b14b44b5121370f02d74de", "major": 999, "minor": 1004, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:f9"}'
        test_data_2 ='{"uuid": "a495bb10c5b14b44b5121370f02d74de", "major": 73, "minor": 913, "tx_power": -59, "rssi": -64, "mac": "f9:8c:ac:b8:2c:f9"}'
        for i in range(3):
            rc = check_tilt_data(test_data_1)
            time.sleep(0.5)
        for i in range(2):
            rc = check_tilt_data(test_data_2)
            time.sleep(0.5)

    if int(test_number) == 2: #unknown mac
        test_data_1 ='{"uuid": "a495bb10c5b14b44b5121370f02d74de", "major": 999, "minor": 1004, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:f8"}'
        rc = check_tilt_data(test_data_1)

    if int(test_number) == 3: #temperature increases
        for t in range(40,130):
            test_data_1 ='{"uuid": "a495bb10c5b14b44b5121370f02d74de", "major":' + str(t) + ', "minor": 1004, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:f9"}'
            rc = check_tilt_data(test_data_1)
            time.sleep(0.5)

    if int(test_number) == 4: #gravity increases, vertical->horizontal
        for t in range(920,1240):
            test_data_1 ='{"uuid": "a495bb10c5b14b44b5121370f02d74de", "minor":' + str(t) + ', "major": 77, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:f9"}'
            rc = check_tilt_data(test_data_1)
            time.sleep(0.5)

    if int(test_number) == 5: #gravity increases, vertical->horizontal, multiple sensors
        for t in range(920,1240):
            test_data_1 ='{"uuid": "a495bb10c5b14b44b5121370f02d74de", "minor":' + str(t) + ', "major": 77, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:f9"}'
            test_data_2 ='{"uuid": "a495bb10c5b14b44b5121370f02d75de", "minor":' + str(t+1) + ', "major": 70, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:fa"}'
            test_data_3 ='{"uuid": "a495bb10c5b14b44b5121370f02d76de", "minor":' + str(t+3) + ', "major": 67, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:fb"}'
            test_data_4 ='{"uuid": "a495bb10c5b14b44b5121370f02d77de", "minor":' + str(t+5) + ', "major": 100, "tx_power": -103, "rssi": -62, "mac": "f9:8c:ac:b8:2c:fc"}'
            rc = check_tilt_data(test_data_1)
            rc = check_tilt_data(test_data_2)
            rc = check_tilt_data(test_data_3)
            rc = check_tilt_data(test_data_4)
            time.sleep(0.5)


    time.sleep(1)
    exit(0)



def my_process(data):
    global opts
    global mqtt_client1
    global config
    global new_data

    ev = aiobs.HCI_Event()
    xx = ev.decode(data)
    if opts.mac:
        goon = False
        mac = ev.retrieve("peer")
        for x in mac:
            if x.val in opts.mac:
                goon = True
                break
        if not goon:
            return

    if opts.raw:
        print("Raw data: {}".format(ev.raw_data))
    if decoders:
        for leader, decoder in decoders:
            xx = decoder.decode(ev)
            if xx:
                new_data = True
                rc = check_tilt_data(xx)
                if config['debug']:
                  if opts.leader:
                    print("{} {}".format(leader, xx))
                  else:
                    print("{}".format(xx))
                  break
    else:
        ev.show(0)


async def amain(args=None):
    global opts
    global new_data

    event_loop = asyncio.get_running_loop()

    # First create and configure a raw socket
    mysocket = aiobs.create_bt_socket(opts.device)

    # create a connection with the raw socket
    # This used to work but now requires a STREAM socket.
    # fac=event_loop.create_connection(aiobs.BLEScanRequester,sock=mysocket)
    # Thanks to martensjacobs for this fix
    conn, btctrl = await event_loop._create_connection_transport(
        mysocket, aiobs.BLEScanRequester, None, None
    )
    # Attach your processing
    btctrl.process = my_process
    if opts.advertise:
        command = aiobs.HCI_Cmd_LE_Advertise(enable=False)
        await btctrl.send_command(command)
        command = aiobs.HCI_Cmd_LE_Set_Advertised_Params(
            interval_min=opts.advertise, interval_max=opts.advertise
        )
        await btctrl.send_command(command)
        if opts.url:
            myeddy = EddyStone(param=opts.url)
        else:
            myeddy = EddyStone()
        if opts.txpower:
            myeddy.power = opts.txpower
        command = aiobs.HCI_Cmd_LE_Set_Advertised_Msg(msg=myeddy)
        await btctrl.send_command(command)
        command = aiobs.HCI_Cmd_LE_Advertise(enable=True)
        await btctrl.send_command(command)
    # Probe
    await btctrl.send_scan_request()
    new_data = False
    counter = 0
    try:
        while True:
            counter += 1
            if counter == 2:
                if not new_data:
                    exit(1)
                counter = 0
            await asyncio.sleep(10)
    except KeyboardInterrupt:
        print("keyboard interrupt")
    finally:
        print("closing event loop")
        # event_loop.run_until_complete(btctrl.stop_scan_request())
        await btctrl.stop_scan_request()
        command = aiobs.HCI_Cmd_LE_Advertise(enable=False)
        await btctrl.send_command(command)
        conn.close()


def main():
    global opts
    global config

    print("Reading config from file")
    tilt2modbus = Tilt2Modbus()
    config = tilt2modbus.config
    run_mqtt_setup(config)

    if len(sys.argv) > 1:
        if sys.argv[1] == "test":
            rc = do_tests(sys.argv[2])
            exit(0)

    parser = argparse.ArgumentParser(description="Track BLE advertised packets")
    parser.add_argument(
        "-e",
        "--eddy",
        action="store_true",
        default=False,
        help="Look specifically for Eddystone messages.",
    )
    parser.add_argument(
        "-m",
        "--mac",
        type=check_mac,
        action="append",
        help="Look for these MAC addresses.",
    )
    parser.add_argument(
        "-r",
        "--ruuvi",
        action="store_true",
        default=False,
        help="Look only for Ruuvi tag Weather station messages",
    )
    parser.add_argument(
        "-A",
        "--atcmi",
        action="store_true",
        default=False,
        help="Look only for ATC_MiThermometer tag messages",
    )
    parser.add_argument(
        "-T",

        "--thermobeacon",
        action="store_true",
        default=False,
        help="Look only for ThermoBeacon messages",
    )
    parser.add_argument(
        "-R",
        "--raw",
        action="store_true",
        default=False,
        help="Also show the raw data.",
    )

    parser.add_argument(
        "-a",
        "--advertise",
        type=int,
        default=0,
        help="Broadcast like an EddyStone Beacon. Set the interval between packet in millisec",
    )
    parser.add_argument(
        "-u",
        "--url",
        type=str,
        default="",
        help="When broadcasting like an EddyStone Beacon, set the url.",
    )
    parser.add_argument(
        "-t",
        "--txpower",
        type=int,
        default=0,
        help="When broadcasting like an EddyStone Beacon, set the Tx power",
    )
    parser.add_argument(
        "-D",
        "--device",
        type=int,
        default=0,
        help="Select the hciX device to use (default 0, i.e. hci0).",
    )
    parser.add_argument(
        "--tilt",
        action="store_true",
        default=False,
        help="Look only for Tilt hydrometer messages",
    )
    parser.add_argument(
        "--skip-leader",
        action="store_false",
        dest="leader",
        help="suppress leading text identifier",
    )
    try:
        opts = parser.parse_args()
    except Exception as e:
        parser.error("Error: " + str(e))

    if opts.eddy:
        decoders.append(("Google Beacon", EddyStone()))
    if opts.ruuvi:
        decoders.append(("Weather info", RuuviWeather()))
    if opts.atcmi:
        decoders.append(("Temperature info", ATCMiThermometer()))
    if opts.thermobeacon:
        decoders.append(("Temperature info", ThermoBeacon()))
    if opts.tilt:
        decoders.append(("Tilt", Tilt()))

    decoders.append(("Tilt", Tilt()))
    try:
        asyncio.run(amain())
    except:
        pass

if __name__ == "__main__":
    main()
